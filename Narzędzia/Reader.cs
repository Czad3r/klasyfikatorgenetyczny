﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domena;


namespace Narzędzia
{
    class Reader
    {
        public List<OriginalIndividual> Read(string path, char separator)
        {

            //string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/aaa.txt";

            List<OriginalIndividual> originalIndividuals = new List<OriginalIndividual>();

            int originalIndividualsAmount = TotalLines(path);

            using (StreamReader sr = new StreamReader(path))
            {

                for (int i = 0; i < originalIndividualsAmount; i++)
                {
                    originalIndividuals.Add(FromText(sr.ReadLine(), separator));
                }

            }

            return originalIndividuals;
        }

        static OriginalIndividual FromText(string text, char separator)
        {

            string[] separatedValueTexts = text.Split(separator);

            List<int> values = new List<int>();
            values.StringTableToIntList(separatedValueTexts);

            return new OriginalIndividual(values);
        }

        static int TotalLines(string filePath)
        {
            using (StreamReader r = new StreamReader(filePath))
            {
                int i = 0;
                while (r.ReadLine() != null) { i++; }
                return i;
            }
        }

    }
}
