﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narzędzia
{
    public static class ListExtension
    {
        public static void StringTableToIntList(this List<int> list, string[] table)
        {
            for (int i = 0; i < table.Length; i++)
            {
                list.Add(int.Parse(table[i]));
            }
        }

    }
}
