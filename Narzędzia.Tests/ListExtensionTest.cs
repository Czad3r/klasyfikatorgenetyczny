﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Narzędzia.Tests
{
    [TestFixture]
    class ListExtensionTest
    {
        private List<int> values;

        [SetUp]
        public void BeforeEveryTest()
        {
            values = new List<int>();
        }

        [Test]
        public void Should_Work_StringTableToIntList_1()
        {
            string text = "1 5 7 7 6 6 6 6 6 5 5 5 5 5 5 5 5 5 5 5 5";
            char separator = ' ';
            string[] separatedValueTexts = text.Split(separator);

            values.StringTableToIntList(separatedValueTexts);

            Assert.That(values.Count, Is.EqualTo(21));
        }

        [Test]
        public void Should_Work_StringTableToIntList_2()
        {
            string text = "1.5.7.7.6.6.6.6.6.5.5.5.5.5.5.5.5.5.5.5.5";
            char separator = '.';
            string[] separatedValueTexts = text.Split(separator);

            values.StringTableToIntList(separatedValueTexts);

            Assert.That(values.Count, Is.EqualTo(21));
        }

        [Test]
        public void Should_Throw_StringTableToIntList_3()
        {
            string text = "h 5 7 7 6 6 6 6 6 5 5 5 5 5 5 5 5 5 5 5 5";
            char separator = ' ';
            string[] separatedValueTexts = text.Split(separator);

            Assert.Throws<FormatException>(() => values.StringTableToIntList(separatedValueTexts));
            Assert.That(values.Count, Is.EqualTo(0));
        }

        [Test]
        public void Should_Throw_StringTableToIntList_4()
        {
            string text = "";
            char separator = ' ';
            string[] separatedValueTexts = text.Split(separator);

            Assert.Throws<FormatException>(() => values.StringTableToIntList(separatedValueTexts));
            Assert.That(values.Count, Is.EqualTo(0));
        }

        [Test]
        public void Should_StringTableToIntList_5()
        {
            string text = "157766666555555555555";
            char separator = ' ';
            string[] separatedValueTexts = text.Split(separator);

            Assert.Throws<OverflowException>(() => values.StringTableToIntList(separatedValueTexts));
            Assert.That(values.Count, Is.EqualTo(0));
        }

    }
}
