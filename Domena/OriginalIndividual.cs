﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domena
{
    public class OriginalIndividual
    {
        private List<int> variables;

        public int Amount => variables.Count;
        public int InputVariables => Amount - 1;

        public List<int> Variables { get; }

        public int ResultVariable => variables[variables.Count - 1];

        public OriginalIndividual(List<int> variables)
        {
            this.variables = new List<int>(variables);
        }

        public List<int> GetInputVariables()
        {
            List<int> inputVariables = new List<int>(variables);
            inputVariables.RemoveAt(inputVariables.Count - 1);
            return inputVariables;
        }
    }
}
